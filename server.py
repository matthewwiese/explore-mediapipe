from http.server import HTTPServer, SimpleHTTPRequestHandler
import ssl

class CORSRequestHandler(SimpleHTTPRequestHandler):
    def end_headers (self):
        self.send_header('Access-Control-Allow-Origin', '*')
        SimpleHTTPRequestHandler.end_headers(self)

httpd = HTTPServer(('0.0.0.0', 8080), CORSRequestHandler)
httpd.socket = ssl.wrap_socket(httpd.socket, keyfile="selfsigned.key", certfile='selfsigned.crt', server_side=False)
httpd.serve_forever()
