# MediaPipe Explorations
<https://gitlab.com/sat-mtl/google-summer-of-code/-/issues/31>
## Table of Contents
* [MediaPipe Holistic Solution Boilerplate Example](./holistic-boilerplate)
* [MediaPipe Extract Nose Landmark](./nose-mouse)

## Running
To listen at `https://0.0.0.0:8080`:
```sh
python server.py
```
HTTPS [is required](https://stackoverflow.com/a/57014083) for `navigator.mediaDevice`. HTTP is only supported via `localhost`.

## Generating a Certificate
To generate a `selfsigned.crt` and `selfsigned.key` for `server.py`:
```
./gen-cert.sh
```
And follow the prompts.

### Manually
Use [lc-tlscert](https://github.com/driskell/log-courier/blob/main/lc-tlscert/lc-tlscert.go):

```sh
wget https://raw.githubusercontent.com/driskell/log-courier/main/lc-tlscert/lc-tlscert.go
go build lc-tlscert.go
./lc-tlscert
```

[Source](https://serverfault.com/a/623538)
