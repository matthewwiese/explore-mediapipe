#!/usr/bin/env sh
wget https://raw.githubusercontent.com/driskell/log-courier/main/lc-tlscert/lc-tlscert.go
go build lc-tlscert.go
./lc-tlscert
rm lc-tlscert
rm lc-tlscert.go
