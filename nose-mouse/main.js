const videoElement = document.getElementsByClassName('input_video')[0];
const canvasElement = document.getElementById('mp-canvas');
const canvasCtx = canvasElement.getContext('2d');

const controls = window;
const drawingUtils = window; 
const mpFaceDetection = window;

function drawDot(p){
    canvasCtx.beginPath();
    canvasCtx.arc(p.x, p.y, 6, 0, 2 * Math.PI, true);
    canvasCtx.fillStyle = "yellow";
    canvasCtx.fill();
}

function drawLine(p1, p2) {
    canvasCtx.beginPath();
    canvasCtx.lineWidth = 4;
    canvasCtx.strokeStyle = "blue";
    canvasCtx.moveTo(p1.x, p1.y);
    canvasCtx.lineTo(p2.x, p2.y);
    canvasCtx.stroke();
}

// Geometry class coming in handy!
function calcDistance(p1, p2) {
    const xQuantity = Math.pow((p2.x - p1.x), 2);
    const yQuantity = Math.pow((p2.y - p1.y), 2);
    return(Math.sqrt(xQuantity + yQuantity));
}

function drawStatus(p1, p2) {
    const distance = calcDistance(p1, p2);
    const horizontalDirection = p1.x > p2.x ? 'left' : 'right';
    const verticalDirection   = p1.y > p2.y ? 'up'   : 'down';

    const statusText = `Magnitude: ${distance} - ${horizontalDirection} ${verticalDirection}`;

    canvasCtx.font = "30px sans";
    canvasCtx.strokeStyle = "blue";
    canvasCtx.strokeText(statusText, 25, canvasElement.height - 25);
}

function onResults(results) {
  // Draw the overlays.
  canvasCtx.save();
  canvasCtx.clearRect(0, 0, canvasElement.width, canvasElement.height);
  canvasCtx.drawImage(
      results.image, 0, 0, canvasElement.width, canvasElement.height);
  if (results.detections.length > 0) {
    const leftEar  = results.detections[0].landmarks[5];
    const rightEar = results.detections[0].landmarks[4];
    const earCenter = {
        x: canvasElement.width * ((rightEar.x + leftEar.x) / 2),
        y: canvasElement.height * ((rightEar.y + leftEar.y) / 2),
    };
    const noseTip = results.detections[0].landmarks[2];
    noseTip.x = noseTip.x * canvasElement.width;
    noseTip.y = noseTip.y * canvasElement.height;
    drawLine(earCenter, noseTip);
    drawDot(noseTip);
    drawStatus(earCenter, noseTip);
  }
  canvasCtx.restore();
}

const faceDetection = new FaceDetection({locateFile: (file) => {
  return ` https://cdn.jsdelivr.net/npm/@mediapipe/face_detection@0.4/${file}`;
}});
faceDetection.setOptions({
  model: 0, // Short range model for faces 2 meters from camera
  minDetectionConfidence: 0.65,
});
faceDetection.onResults(onResults);

(async () => {
  await faceDetection.initialize();
})();

const camera = new Camera(videoElement, {
  onFrame: async () => {
    await faceDetection.send({image: videoElement});
  },
  width: 1280,
  height: 720
});
camera.start();